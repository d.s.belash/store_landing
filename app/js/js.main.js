jQuery(function () {
  //variables

  var removeBtns = $('.remove-product'),
    deleteBtns = $('.delete-product-btn'),
    cartBtns = $('.to-cart-button'),
    clearList = $('#clearlist-btn'),
    productSearch = $('#product-search'),
    userEmail = $('#user-email'),
    buyButton = $('#buy-button'),
    productsCounter = $('#products-counter'),
    sum = 0,
    goodsCounter = 0,
    regExp = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,//простая проверка для примера
    options = {
      valueNames: ['id', 'product-name'],
      page: 8,
      pagination: true,
      dotted: false,

    },
    userList = new List('products', options);

  //slider initiation

  $('#features-slider').slick({
    dots: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1500,
    arrows: false,
    focusOnSelect: false,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 3,
        centerMode: true,
        centerPadding: '0px'
      }
    },
      {
        breakpoint: 768,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 2,
          centerMode: true,
          centerPadding: '0px'
        }
      },
      {
        breakpoint: 540,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1,
          centerMode: true,
          centerPadding: '0px'
        }
      },

    ]
  });


  deleteBtns.on('click', function (e) {
    deleteItem(e, $(this), userList);
  });

  removeBtns.on('click', function (e) {
    deleteItem(e, $(this), userList);
  });


  //filter

  if ($(productSearch).val() === '') {
    $(clearList).prop('disabled', true);
  }

  $(productSearch).keyup(function () {
    if ($(this).val() === '') {
      $(clearList).prop('disabled', true);
      $(clearList).addClass('disabled');
    } else {
      $(clearList).prop('disabled', false);
      $(clearList).removeClass('disabled');
    }
  });

  $(clearList).on('click', function () {
    resetList();
    $(clearList).prop('disabled', true);
  });

  $('#search-btn').on('click', function () {
    $($(this).find('.icon-search')[0]).toggleClass('active-search');
    $('#product-search').toggle('slow');
  });

  //validation

  $(userEmail).on('focus', function () {
    $(this).removeClass('input-error');
    $(this).closest('form').removeClass('incorrect');
  });

  $(userEmail).on('blur', function () {
    checkField($(this), regExp);
  });

  $('form').on('submit', function (e) {
    e.preventDefault();
    let resultsArr = [];
    var inputsArr = $(this).find('input[type="text"]');
    for (var i = 0; i < inputsArr.length; i++) {
      resultsArr.push(checkField($(inputsArr[i]), regExp));
    }
    if (resultsArr.some((i) => {
      return i === false
    })) {
      return false;
    }
    else {
      //     $(this).submit()//отправляем форму, типа
      location.reload();
    }
  });

  $(cartBtns).on('click', function (e) {

    e.preventDefault();
    var price = $(this).closest('.product-wrapper').find('.product-price-value').text();

    var isClear = price.indexOf('$');
    if (isClear !== -1) {
      price = price.substring(isClear + 1,);
    }
    price = parseFloat(price);
    sum += price;
    goodsCounter++;
    $(buyButton).trigger('sumHasChanged');
  });

  $(productsCounter).on('click', function () {
    sum = 0;
    goodsCounter = 0;
    $(buyButton).trigger('sumHasChanged');
  });

  $(buyButton).on('sumHasChanged', function () {
    $('#sum-price').text('$ ' + sum + '.00');
    $(productsCounter).text(goodsCounter);
  });

  function checkField(field, regExp) {
    if (regExp.test(field.val()) === false) {
      field.addClass('input-error');
      field.closest('form').addClass('incorrect');
      return false
    }
    return true
  };

  function resetList() {
    userList.search();
    userList.filter();
    userList.update();
    $(productSearch).val('');
  };

  function deleteItem(e, self, userList) {
    e.preventDefault();
    var itemId = $(self).closest('.product-wrapper').find('.id').text();
    userList.remove('id', itemId);
  }
});




